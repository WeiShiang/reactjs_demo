import React from 'react';
import { withFormik, Form, Field, ErrorMessage } from "formik";
import * as yup from 'yup'

const errMsg = {  // 錯誤訊息的style
    color: 'red',
    fontSize: '12px',
    paddingLeft: '5px'
  };
  
  // 表單內容 ▼
  const BicycleFormFuntion = ({
    values,
    isSubmitting
  }) => (
    <Form>
      <div>
        <Field name="city" placeholder="city"/>
        <ErrorMessage name="city" component="span" style={errMsg}/>
      </div>
      <div>
        <Field name="top" placeholder="top"/>
        <ErrorMessage name="top" component="span" style={errMsg}/>
      </div>
      <div>
        <label>
          <Field type="checkbox" name="rule" checked={values.rule}/>
          同意註冊<a href="#">條款</a>
        </label>
        <ErrorMessage name="rule" component="span" style={errMsg}/>
      </div>
      <button type="submit" disabled={isSubmitting}>送出</button>
    </Form>
    
  )
  
  export default withFormik({
    // input 預設值
    // Formik 將表單的內部狀態或處理結果轉成 props 讓元件取用，
    // 若不指定則 Formik 只會將資料型別不是函式的部份轉成 props 讓表單取用
    mapPropsToValues({ city, top, rule }) {
      return {
        city: city || '',
        top: top || '',
        rule: rule || false
      }
    },
    // 表單驗證條件＆錯誤訊息
    validationSchema: yup.object().shape({
      city: yup.string().min(6, '密碼至少大於6').required('必填'),
      top: yup.number().min(1, '至少一位').required('必填'),
      rule: yup.boolean().oneOf([true], '一定要同意！'),
    }),
    // 點擊送出時
    handleSubmit(values, { resetForm, setSubmitting }) {
      setTimeout(() => {
        resetForm();  //重設表單
        setSubmitting(false); //狀態更新(true:傳送中, false:傳送完成)
        alert(JSON.stringify(values, null, 2));  //alert values
      }, 1000)
    }
  })(BicycleFormFuntion)